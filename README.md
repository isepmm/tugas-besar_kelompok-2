# Tugas Besar Kelompok 2

MUMU : Cloud Storage and Cloud Messager (Socket dan Remote Prosedur Control)

# Fungsionalitas : 
    1. Server   : 
    
            - Storage  : 1. penyimpanan data
            
            - Messager : 1. menyimpan pesan 
                         2. menyampaikan pesan 
                         3. penjadwalan pesan 
                         
    2. Client   : 
    
            - Storage   : 1. upload file atau data
                          2. download file atau data 
            
            - Message   : 1. menerima pesan 
                          2. mengirim pesan 
                          
# Pembagian Tugas :
    1. Isep     :  1. bikin repository gitlab 
                   2. coding client
                   
    2. Habibie  :  1. coding client 
    
    3. Arlinda  :  1. markdown
                   2. buat wiki di gitlab
                   
    4. Andi     :  1. coding server 
                   2. inisialisasi awal
                   
# Alur Program pada Aplikasi Cloud Storage 
    1. User mengupload file, lalu server menerima file yang diupload oleh user tersebut
    2. User mendownload file, maka server mengirim file yang ingin di download oleh user
    
![S__56926447](/uploads/5a854e76c38e6b3622837fe81a94a3e7/S__56926447.jpg)
    
# Alur Program pada Aplikasi Cloud Messager 
    1. Admin mengirim topik A 
    2. Server menyimpan topik A tersebut
    3. Client meminta topik A
    4. Server mengirim topik A sesuai yang diinginkan Client
    5. Client menerima topik A
    
![859](/uploads/329b5f5d139406f049c139bdc6cf205a/859.jpg)

# Update Features

## 1. Isep Mumu Mubaroq

- Features Update Topic
  1. Admin mengirim topik A
  2. Server menyimpan topik A tersebut
  3. Admin mengupdate topik A menjadi Topik B
  4. Server menyimpan topik B tersebut

## 2. Fikri Habibie

- Feature Update File in Cloud Storage
  1. Client melakukan request untuk mendownload file dari server
  2. Server server mengirim file ke client
  3. client dapat melakukan update jika client memiliki file yang ingin didownload dan server menyediakan file yang ingin diupdate oleh client
   
## 3. Arlinda Dwi Ardiyani

- Feature Backup File in Cloud Storage 
  1. User mengupload file
  2. Server menerima file yang diupload dan langsung menyimpan serta mengbackup file tersebut.
  
## 4. Andi Waluyo

- Menambahkan fitur Import dan Export Topic yang sudah tersedia
  1. Download Topic menjadi sebuah file.txt
  2. Topic yang sudah di download bisa di export ke dalam Server sehingga topic tidak perlu di tambahkan satu persatu
  3. Kendala : Belum selesai pemanggilan List Dictionary, sehingga belum bisa di uji coba
    