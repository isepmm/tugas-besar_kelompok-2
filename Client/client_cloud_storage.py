import socket
import os

server_ip = "192.168.0.102"
server_port = 2500
buffer = 4096

def request(data, sock):
	sock.send(data.encode("utf-8"))
	namafile = sock.recv(buffer)
	return namafile

def upload(data, sock):
	reply = request(data, sock).decode("utf-8")

	try:
		file = open(data, "rb")
		byte = file.read(buffer)

		while byte != b'':
			sock.send(byte)
			byte = file.read(buffer)

	finally:
		file.close()
		print(reply, "berhasil diupload")
		print("\n")
	sock.close()

def download(data, sock):
	reply = request(data, sock).decode("utf-8")

	if  reply == "file tidak tersedia":
		print(reply)
		print("\n")

	else:
		if(os.path.isfile(data)): 
			namafile = "(1)" + data  
		else:
			namafile = data

		file = open(namafile, 'wb')

		while 1:
			byte = sock.recv(buffer)
			file.write(byte)
			if not byte: break
    
		print(reply," berhasil didownload")
		print("\n")
		file.close()

	sock.close()

def update(data, sock):
	reply = request(data, sock).decode("utf-8")

	if  reply == "file tidak tersedia":
		print(reply)
		print("\n")

	else:
		file = open(data, 'wb')
	
		while 1:
			byte = sock.recv(buffer)
			file.write(byte)
			if not byte: break
    
		print(reply," berhasil diupdate")
		print("\n")
		file.close()

	sock.close()
	
def show_menu():
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.connect((server_ip, server_port))

	print ("Menu Pilihan")
	print ("1. Download")
	print ("2. upload")
	print ("3. update file")
	print ("4. Exit")
	
	choice = input("Pilih Menu : ")
	print("\n")
	
	if choice == "1":
		pesan = input("File yang didownload : ")
		print("\n")
		download(pesan, sock)
		show_menu()
		
	elif choice == "2":
		pesan = input("File yang diupload : ")
		print("\n")
		
		if(os.path.isfile(pesan)):
			upload(pesan, sock)
		else:
			print("file tidak ditemukan")
			print("\n")
			sock.close()
		show_menu()

	elif choice == "3":
		pesan = input("File yang diupdate : ")
		print("\n")

		if(os.path.isfile(pesan)):
			update(pesan, sock)
		else:
			print("file tidak ditemukan")
			print("\n")
			sock.close()
		show_menu()

	elif choice == "4":
		print("Exit")
		sock.close()
		exit()
		
	else:
		print ("SALAH PILIH !!")
		show_menu()
	
show_menu()