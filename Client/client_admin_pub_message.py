import time
# import xmlrpc bagian client saja
import xmlrpc.client
import json
import os

# buat stub (proxy) untuk client
stub = xmlrpc.client.ServerProxy('http://192.168.1.16:8787')

def show_menu():
    print ("Menu Pilihan")
    print
    print ("1. Buat Topik")
    print ("2. Update Topik")
    print ("3. Push Messager")
    print ("4. Download Topic")
    print ("5. Load topic")
    print ("6. Exit")
    print
    choice = input("Pilih Menu : ")
    print ("\n")

    if choice == "1":
        choice1 = input("Masukkan Topik : ")
        stub.topic_register(str(choice1))
        print("Berhasil menambahkan topik baru\n")
        show_menu()
    elif choice == "2":
        dict = stub.load_topic()
        if(len(dict) < 1):
            print("Belum Ada Topik Yang Dibuat")
            show_menu()
        else:
            counter = 0
            dictionary = []
            for key, value in dict.items():
                dictionary.append(key)
                print(counter,".",key)
                counter += 1
            print("")
            choice3 = input("Masukkan Topik yang Diupdate : ")
            try:
                topik_pil = dictionary[int(choice3)]
                update = input(topik_pil + " => " )
                print(stub.update_topic(update,topik_pil))
                print("\n")
                show_menu()
            except:
                print("inputan salah")
                show_menu()
    elif choice == "3":
        dict = stub.load_topic()
        if(len(dict) < 1):
            print("Belum Ada Topik Yang Dibuat, bBat Pada Menu Nomor 1\n")
            show_menu()
        else:
            counter = 0
            dictionary = []
            for key, value in dict.items():
                dictionary.append(key)
                print(counter,".",key)
                counter += 1
            print("")
            choice2 = input("Pilih Topik Di Atas : ")
            try:
                topik_pilihan = dictionary[int(choice2)]
                print("TOPIK : ",topik_pilihan)
                print("\n")
                message = input("Masukkan Pesan : ")
                stub.send_message(str(topik_pilihan),str(message))
                print("\n")
                print("Pesan Terkirim")
                show_menu()
            except:
                print("Inputan Salah")
                show_menu()
    elif choice == "4":
        dict = stub.load_topic()
        if(len(dict) < 1):
            print("Belum Ada Topik Yang Dibuat, Buat Pada Menu Nomor 1\n")
            show_menu()
        else:
            list = stub.dl_topic()
            with open('file.txt', 'w') as file:
                file.write(json.dumps(list)) # use `json.loads` to do the reverse
            print("Export Topic success")
            print("")
            show_menu()
    elif choice == "5":
        topic = {}
        with open('file.txt', 'r') as file:
            topic = json.load(file)
        stub.rt_topic(topic)
        print ("Import Topic Success")
        print("")
        show_menu()
    else:
        print ("SALAH PILIH !!")
        show_menu()

show_menu()
