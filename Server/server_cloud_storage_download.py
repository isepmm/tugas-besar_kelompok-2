import socket
import os

server_ip = '192.168.0.102'
server_port = 2500
buffer = 4096

while 1:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.bind((server_ip, server_port))

	sock.listen(1)

	conn, addr = sock.accept()
	pesan = conn.recv(buffer)
	
	if(os.path.isfile(pesan)): 

		conn.send(pesan)

		try:
			file = open(pesan.decode("utf-8"), "rb")
			byte = file.read(buffer)
    
			while byte != b'':
				conn.send(byte)
				byte = file.read(buffer)
        
		finally:
			file.close()
	
			print("file dikirim")
			print("ke : ", addr)

	else:
		NotAvailable = "file tidak tersedia" 
		conn.send(NotAvailable.encode("utf-8"))
		
	conn.close()
	sock.close()