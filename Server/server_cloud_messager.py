import time

# import library SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer

# import xmlrpc bagian client
import xmlrpc.client

SERVER_IP   = "192.168.1.16"
SERVER_PORT = 8787

server = SimpleXMLRPCServer((SERVER_IP,SERVER_PORT),allow_none=True)

topic_message = {}

current_message = ['initial',' ']

def new_topic_register(topic):
    if topic in topic_message:
        return "Topic " + topic + " sudah terdaftar"
    else:
        topic_message[topic] = " "
        return "Topic " + topic + " berhasil di daftarkan"

def update_topic(new_topic,old_topic):
        if old_topic in topic_message:
                topic_message[new_topic] = topic_message.pop(old_topic)
                return "Update Berhasil"
        else:
                return "Update Gagal"

def load_registered_topic():
    return topic_message

def send_message(topic,message):
    topic_message[topic] = message

def receive_message(topic):
    return topic_message[topic]

def reset_message():
    for data,value in topic_message.items():
        topic_message[data] = " "

def download_topic():
    return topic_message

def retreive_topic(new_list):
    topic_message = new_list
    print(topic_message)
    print(new_list)

server.register_function(new_topic_register,'topic_register')
server.register_function(load_registered_topic,'load_topic')
server.register_function(send_message,'send_message')
server.register_function(receive_message,'receive_message')
server.register_function(reset_message,'reset_message')
server.register_function(update_topic,'update_topic')
server.register_function(download_topic,'dl_topic')
server.register_function(retreive_topic,'rt_topic')

print("SERVER IS RUNNING. . .")

server.serve_forever()
