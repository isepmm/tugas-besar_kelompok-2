import socket
import os

server_ip = "192.168.0.102"
server_port = 2500
buffer = 4096

while 1:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.bind((server_ip, server_port))

	sock.listen(1)

	conn, addr = sock.accept()
	pesan = conn.recv(buffer)
	conn.send(pesan)

	folderName = "Backup"

	if not os.path.exists(folderName):
		backup = os.mkdir(folderName)
		file_backup = open(os.path.join(folderName, pesan.decode("utf-8")), "wb+")
	else:
		
		file_backup = open(os.path.join(folderName, pesan.decode("utf-8")), "wb+")

	file = open(pesan.decode("utf-8"), "wb")

	while 1:
		data = conn.recv(buffer)
		file.write(data)
		file_backup.write(data)
    
		if not data: break
	
	file.close()
	file_backup.close()
	print("dari : ", addr)
	print("file ", pesan.decode("utf-8")," diterima")

	conn.close()
	sock.close()